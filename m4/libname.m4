dnl -*- Autoconf -*- Determine platform-specific name of library.

dnl Copyright (C) 2017 The Qt Company, Ltd
dnl This file is free software; the Qt Company
dnl gives unlimited permission to copy and/or distribute it,
dnl with or without modifications, as long as this notice is preserved.

dnl $1 is the library basename, $2 the library version.
dnl Defines lib[$1]_BARE, lib[$1]_SONAME, and lib[$1]_VERSIONED AC_SUBST.

AC_DEFUN([eu_LIBNAME], [dnl

name="$1"
version="$2"

AC_CACHE_CHECK([bare name of $1 lib], [eu_cv_sys_lib[$1]_bare], [
eu_cv_sys_lib[$1]_bare=$LIBPREFIX$name$LIBEXT
])
lib[$1]_BARE=$eu_cv_sys_lib[$1]_bare
AC_SUBST([lib$1_BARE])

AC_CACHE_CHECK([versioned name of $1 lib], [eu_cv_sys_lib[$1]_versioned], [
eu_cv_sys_lib[$1]_versioned=$LIBPREFIX$name-$PACKAGE_VERSION$LIBEXT
])
lib[$1]_VERSIONED=$eu_cv_sys_lib[$1]_versioned
AC_SUBST([lib$1_VERSIONED])

AC_CACHE_CHECK([soame of $1 lib], [eu_cv_sys_lib[$1]_soname], [
if test "$eu_postfix_lib_version" = "yes"; then
eu_cv_sys_lib[$1]_soname=$LIBPREFIX$name$LIBEXT.$version
else
eu_cv_sys_lib[$1]_soname=$LIBPREFIX$name$version$LIBEXT
fi
])
lib[$1]_SONAME=$eu_cv_sys_lib[$1]_soname
AC_SUBST([lib$1_SONAME])

])
